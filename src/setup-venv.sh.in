#!/usr/bin/env bash

source @TTB_LIB@

ttb_usage() {
  echo "Usage:"
  echo -e "\t${TTB_EXE} $(basename $0)"
}

ttb_describe() {
  echo "Install the venv for @TTB_MODULE@ using PyTango @TTB_PYTANGO_VERSION@."
}

ttb_handle_standard_args "$@"

if [ "${#TTB_ARGS[@]}" -ne 0 ]; then
  ttb_usage >&2
  exit 1
fi

set -e

if ! [ -e "${TTB_ROOT}" ]; then
  "@TTB_CMD_SCRIPTS_DIR@/fetch"
fi

ttb_log_info "Installing venv for @TTB_MODULE@..."

set -x

cd "${TTB_ROOT}"

export POETRY_VIRTUALENVS_IN_PROJECT=true

poetry env use @TTB_PYTHON_VERSION@
poetry source add --priority=explicit ttb @TTB_PYPI@
# TODO: Handle artifacts from some pytango CI pipeline run.
poetry add --group=dev pytango==@TTB_PYTANGO_VERSION@ @TTB_INSTALL_DEPS@
