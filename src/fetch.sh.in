#!/usr/bin/env bash

source @TTB_LIB@

ttb_usage() {
  echo "Usage:"
  echo -e "\t${TTB_EXE} $(basename $0)"
}

ttb_describe() {
  cat << EOF
@TTB_FETCH_DESC@

If @TTB_MODULE@ has already been fetched, it will be reset to a clean state.
EOF
}

ttb_handle_standard_args "$@"

if [ "${#TTB_ARGS[@]}" -ne 0 ]; then
  ttb_usage >&2
  exit 1
fi

set -e

ttb_log_info "Fetching @TTB_MODULE@..."

set -x

if [ -e "${TTB_ROOT}" ]; then
  cd "${TTB_ROOT}"
  git reset --hard @TTB_TAG@
  git clean -f
  if [ -e "${TTB_ROOT}/.venv" ]; then
    rm -rf "${TTB_ROOT}/.venv"
  fi
else
  git -c advice.detachedHead=false clone \
      --recursive --depth 1 --branch @TTB_TAG@ \
      @TTB_URL@ "${TTB_ROOT}"
  cd "${TTB_ROOT}"
fi

for patch in $(find "${TTB_PATCHES}" -type f | sort 2>/dev/null); do
    git am ${patch}
done

git submodule update

echo -e "release=0.0.0\ntag=0.0.0" > .release

@TTB_FETCH_PATCH_FRAGMENT@

git commit -a -m "TTB PATCH"
