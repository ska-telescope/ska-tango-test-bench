#!/usr/bin/env bash

source @TTB_LIB@

ttb_usage() {
  echo "Usage:"
  echo -e "\t${TTB_EXE} $(basename $0)"
}

ttb_describe() {
  echo "Creates a myvalues.yaml for @TTB_MODULE@, using our images."
}

ttb_handle_standard_args "$@"

if [ "${#TTB_ARGS[@]}" -ne 0 ]; then
  ttb_usage >&2
  exit 1
fi

# Takes a yaml document from standard in and writes a new yaml document to
# stdout. The new yaml document updates all the images which we have built to
# point to our versions.
#
# For example:
#
#   deviceServers:
#     tangotest:
#       image:
#         registry: artefact.skao.int
#         image: ska-tango-images-pytango-runtime
#         tag: 9.5.0
#         pullPolicy: IfNotPresent
#
#   mydeviceserver:
#     ... # Some other keys
#     image:
#       registry: artefact.skao.int
#       image: ska-tango-images-pytango-runtime
#       tag: 9.5.0
#       pullPolicy: IfNotPresent
#     ... # Some other keys
#
#   myotherdeviceserver:
#     ... # Some other keys
#     image:
#        registry: artefact.skao.int
#       image: an-image-we-dont-build
#       tag: some-tag
#       pullPolicy: IfNotPresent
#     ... # Some other keys
#
#   image:
#     registry: artefact.skao.int
#     name: ska-low-mccs-pasd
#     tag: ~
#     pullPolicy: IfNotPresent
#
# becomes:
#
#   deviceServers:
#     tangotest:
#       image:
#         registry: @TTB_RESGIRTY@
#         image: ska-tango-images-pytango-runtime
#         tag: @TTB_COMMIT@
#
#   mydeviceserver:
#     image:
#       registry: @TTB_RESGIRTY@
#       image: ska-tango-images-pytango-runtime
#       tag: @TTB_COMMIT@
#
#   image:
#     registry: @TTB_RESGIRTY@
#     name: ska-low-mccs-pasd
#     tag: @TTB_COMMIT@
#
ttb_updated_chart_images() {
  yq '.. | select(
      tag == "!!map" and key == "image"
      and (has("image") or has("name")) and (.image // .name | tag == "!!str")
      and [ .image // .name ] - @TTB_IMAGE_YQ_ARRAY@ | length == 0
    ) as $image ireduce({};
    setpath($image | path;
      { "image": $image.image // $image.name, "tag": "@TTB_COMMIT@", "registry": "@TTB_OCI_REGISTRY@"}
    ))'
}

set -e

if ! [ -e "${TTB_ROOT}" ]; then
  "@TTB_CMD_SCRIPTS_DIR@/fetch"
fi

cd "${TTB_ROOT}"

ttb_log_info "Creating myvalues.yaml for @TTB_MODULE@..."

set -x

@TTB_HOOK_BEFORE_MAKE_MYVALUES@

@TTB_MYVALUES_OVERRIDES_FRAGMENT@

if ! [ -z "${CI_JOB_TOKEN}" ]; then
  yq --null-input "{ \"global\": { \"cluster_domain\": \"techops.internal.skao.int\" }}" | yq eval-all --inplace 'select(fileIndex == 0) * select(fileIndex == 1)' myvalues.yaml -
fi

