# ska-tango-test-bench

Test bench for testing Tango release candidates against SKA software.

Currently only supports running against ska-tango-base.

## Getting started

When the CI reports an issue with running a <module> tests.

Create a test bench for the module in question:

```bash
cmake -Bbuild -S. -DTTB_MODULE=<module> [-DTTB_COMMIT=<short-sha>]
cmake --build build
```

The configure command accepts the following options:

- `TTB_MODULE` - The name module to configure for testing, required.  If omitted
a list of available modules will be printed
- `TTB_COMMIT` - The commit of ska-tango-test-bench to use to get dependency
artefacts. This is required if the configured module has dependencies.

Then enter the test bench:

```bash
source <(build/test_bench/ttb env)
```

This sets up a `ttb` alias so you can easily run test bench commands.  It also
sets the following environment variables:

- `TTB_MODULE` - The module that is configured for testing, e.g. ska-tango-base
- `TTB_REPO`- The path to the Gitlab repository under ska-telescope, e.g. ska-tango-base
- `TTB_TAG` - The git tag to test against, e.g. 0.19.3
- `TTB_VERSION` - The artefact versions that are generated
- `TTB_ROOT` - The path where the module is checked out
- `TTB_PATCHES` - The path to the directory containing the module's patches

## Commands

The `ttb` alias supports the following commands for running different steps of
the testing process.

- `fetch` - Fetch the module to `$TTB_ROOT` at `$TTB_TAG` and apply any
  patches. If the git repository already exists, will reset to `$TTB_TAG` and
  apply the patches again.
- `python-test` - Run the module "make python-test" tests against the version of pytango configured
- `python-build` - Builds python wheel so dependent modules can use this module
- `oci-build` - Builds Docker images for this module.  Use
  `eval $(minikube -p minikube docker-env)` first to have the images available
  to minikube.
- `helm-build` - Builds the helm charts for this module.
- `make-myvalues` - Create the myvalues.yaml file which specifies the images we
build.  Use `ttb show myvalues` to see the contents.
- `k8s-install` - Install the helm charts for this module. Waits for the charts
to be active.
- `k8s-test` - Run the k8s-tests for this module.
- `k8s-stop` - Uninstall the helm charts for this module and remove k9s
resources.

Note, modules without python packages will not have the `python-test` and
`python-build` commands.  Similar modules without helm charts will not have the
`{oci,helm}-build` and `k8s-*` commands.

Use `ttb help` to see a list of commands that are available for the selected
module.

These step commands will run any dependent steps if required.
e.g. `python-test` depends on `fetch` and will run `fetch` if `TTB_ROOT` does
not exist.

In addition the `ttb` alias supports commands to help debugging issues:

- `help` - Get help on a command.
- `make-patches` - Create TTB_PATCHES for the configured module.
- `env` - Print commands to enter the Test Bench Environment.
- `show` - Show a command script.

## Usage example

1. Run the tests against PyTango 9.5.0:

```bash
ttb python-test
```

2. Go to the module directory:

```bash
cd $TTB_ROOT
```

3. Fix the issue and commit them to the module repository.

4. Show the git log for your commits:

```bash
git log $TTB_TAG..
```

5. When happy, create patches for your module:

```bash
ttb make-patches
```

6. Commit the patch files to your ska-tango-test-bench branch and push to gitlab.

7. Leave the Test Bench Environment

```bash
ttb leave
```

## Generate .gitlab-ci.yml

The gitlab-ci.yml is generated from the module definitions specified in
cmake/ttb_module_definitions.cmake.  To re-generate it run

```
cmake -P cmake/ttb_generate_gitlab-ci.cmake
```

and commit the changes to .gitlab-ci.yml.
