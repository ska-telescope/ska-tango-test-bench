list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})

include(ttb_module_definitions)

set(TTB_JOBS "")
foreach(module ${TTB_MODDEF_AVAILABLE_MODULES})
  name_to_id(${module} id)

  if(TTB_JOBS)
    string(APPEND TTB_JOBS "\n")
  endif()

  if (NOT TTB_MODDEF_${id}_SKIP_PYTHON)
    string(APPEND TTB_JOBS "\
python-build-${module}:\n\
  extends: .python-build-template\n\
  variables:\n\
    TTB_MODULE: ${module}\n\
\n")

    string(APPEND TTB_JOBS "\
python-test-${module}:\n\
  extends: .python-test-template\n\
")

    if (TTB_MODDEF_${id}_DEPS)
      string(APPEND TTB_JOBS "\
  needs:\n")
      foreach(dep ${TTB_MODDEF_${id}_DEPS})
        name_to_id(${dep} depid)
        if (NOT TTB_MODDEF_${depid}_SKIP_PYTHON)
          string(APPEND TTB_JOBS "\
    - python-build-${dep}\n")
        endif()
      endforeach()
    endif()

    string(APPEND TTB_JOBS "\
  variables:\n\
    TTB_MODULE: ${module}\n\
\n")
  endif()

  if (NOT TTB_MODDEF_${id}_SKIP_HELM)
    string(APPEND TTB_JOBS "\
helm-build-${module}:\n\
  extends: .helm-build-template\n\
")

    if (TTB_MODDEF_${id}_DEPS)
      string(APPEND TTB_JOBS "\
  needs:\n")
      foreach(dep ${TTB_MODDEF_${id}_DEPS})
        name_to_id(${dep} depid)
        if (NOT TTB_MODDEF_${depid}_SKIP_HELM)
          string(APPEND TTB_JOBS "\
    - helm-build-${dep}\n")
        endif()
      endforeach()
    endif()

    string(APPEND TTB_JOBS "\
  variables:\n\
    TTB_MODULE: ${module}\n\
\n")

  endif()

  if (NOT TTB_MODDEF_${id}_SKIP_OCI)
    string(APPEND TTB_JOBS "\
oci-build-${module}:\n\
  extends: .oci-build-template\n\
")
    if (TTB_MODDEF_${id}_DEPS)
      string(APPEND TTB_JOBS "\
  needs:\n")
      foreach(dep ${TTB_MODDEF_${id}_DEPS})
        name_to_id(${dep} depid)
        if (NOT TTB_MODDEF_${depid}_SKIP_PYTHON)
          string(APPEND TTB_JOBS "\
    - python-build-${dep}\n")
        endif()
        if (NOT TTB_MODDEF_${depid}_SKIP_OCI)
          string(APPEND TTB_JOBS "\
    - oci-build-${dep}\n")
        endif()
      endforeach()
    endif()
    string(APPEND TTB_JOBS "\
  variables:\n\
    TTB_MODULE: ${module}\n\
\n")
  endif()

  if (NOT TTB_MODDEF_${id}_SKIP_K8S)
    string(APPEND TTB_JOBS "\
k8s-test-${module}:\n\
  extends: .k8s-test-template\n\
  needs:\n\
")
  if (NOT TTB_MODDEF_${id}_SKIP_HELM)
    string(APPEND TTB_JOBS "\
    - helm-build-${module}\n\
")
  endif()
  if (NOT TTB_MODDEF_${id}_SKIP_OCI)
    string(APPEND TTB_JOBS "\
    - oci-build-${module}\n\
")
  endif()
    string(APPEND TTB_JOBS "\
  variables:\n\
    TTB_MODULE: ${module}\n\
\n")

    string(APPEND TTB_JOBS "\
stop-k8s-test-${module}:\n\
  extends: .stop-k8s-test-template\n\
  variables:\n\
    TTB_MODULE: ${module}\n\
\n")
  endif()
endforeach()

configure_file(${CMAKE_CURRENT_LIST_DIR}/gitlab-ci.yml.in ${CMAKE_CURRENT_LIST_DIR}/../.gitlab-ci.yml @ONLY)
