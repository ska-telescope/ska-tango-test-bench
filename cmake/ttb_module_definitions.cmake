# Module definitions

set(TTB_MODDEF_AVAILABLE_MODULES "")
set(TTB_IMGDEF_AVAILABLE_IMAGES "")
set(TTB_CHRTDEF_AVAILABLE_CHARTS "")

#[=======================================================================[.rst:
.. command:: name_to_id

  Store in <outvar> the id corresponding to <name>

  .. signature:: name_to_id(<name> <outvar>)

  :param <name>: Name to convert
  :param <outvar>: Variable to store result in

  Example:

    name_to_id("ska-tango-base" id)
    message("${id}") # SKA_TANGO_BASE
#]=======================================================================]
function(name_to_id name out)
  string(REPLACE "-" "_" tmp ${name})
  string(TOUPPER ${tmp} id)
  set(${out} ${id} PARENT_SCOPE)
endfunction()

#[=======================================================================[.rst:
.. command:: define_module

  .. signature:: define_module(<module>
                               TAG <tag>
                               [VERSION <version>]
                               [DEPS <dep>...]
                               [REPO <repo>]
                               [SUBPROJECT <project>]
                               [IMAGES <image>...]
                               [CHARTS <chart>...]
                               [TEST_CHART <test_chart>]
                               [TEST_IMAGE <test_image>]
                               [CHARTS <chart>...]
                               [USE_HELMFILE]
                               [SKIP_PYTHON]
                               [SKIP_HLEM]
                               [SKIP_OCI]
                               [SKIP_K8S])

   If SKIP_PYTHON is set, does not run python tests or build python artefacts.

   If SKIP_HELM is set, does not build charts.

   If SKIP_OCI is set, does not build docker images.

   If SKIP_K8S is set, does not run k8s tests.

  :param <module>: Name of the module
  :param <tag>: Git tag to test against
  :param <version>: Version of artefacts, defaults to tag
  :param <dep>: Name of module dependency
  :param <image>: Image to build -- defaults to repo name
  :param <repo>: Gitlab repository in the ska-telescope project, if omitted defaults to <module>
  :param <subproject>: Gitlab subproject under ska-telescope
  :param <test_chart>: Chart used for testing -- defaults to repo name
  :param <test_image>: Image to use for k8s-test -- defaults to repo name
  :param <chart>: Chart built by the module -- defaults to repo name
#]=======================================================================]
function(define_module name)
  cmake_parse_arguments(A "SKIP_PYTHON;SKIP_OCI;SKIP_HELM;SKIP_K8S" "REPO;SUBPROJECT;TAG;VERSION;TEST_CHART;TEST_IMAGE;HELMFILE_ENV" "DEPS;IMAGES;CHARTS" ${ARGN})

  name_to_id(${name} id)

  if (A_TAG)
    set(TTB_MODDEF_${id}_TAG ${A_TAG} PARENT_SCOPE)
  else()
    message(FATAL_ERROR "TAG is required")
  endif()

  if (A_VERSION)
    set(TTB_MODDEF_${id}_VERSION ${A_VERSION} PARENT_SCOPE)
  else()
    set(TTB_MODDEF_${id}_VERSION ${A_TAG} PARENT_SCOPE)
  endif()

  if (A_REPO)
    set(TTB_MODDEF_${id}_REPO ${A_REPO} PARENT_SCOPE)
  else()
    set(TTB_MODDEF_${id}_REPO ${name} PARENT_SCOPE)
    set(A_REPO ${name})
  endif()

  if (A_SUBPROJECT)
    set(TTB_MODDEF_${id}_SUBPROJECT ${A_SUBPROJECT} PARENT_SCOPE)
  endif()

  if (A_SKIP_PYTHON)
    set(TTB_MODDEF_${id}_SKIP_PYTHON TRUE PARENT_SCOPE)
  else()
    set(TTB_MODDEF_${id}_SKIP_PYTHON FALSE PARENT_SCOPE)
  endif()

  if (A_SKIP_OCI)
    set(TTB_MODDEF_${id}_SKIP_OCI TRUE PARENT_SCOPE)
  else()
    set(TTB_MODDEF_${id}_SKIP_OCI FALSE PARENT_SCOPE)
  endif()

  if (A_SKIP_HELM)
    set(TTB_MODDEF_${id}_SKIP_HELM TRUE PARENT_SCOPE)
  else()
    set(TTB_MODDEF_${id}_SKIP_HELM FALSE PARENT_SCOPE)
  endif()

  if (A_SKIP_K8S)
    set(TTB_MODDEF_${id}_SKIP_K8S TRUE PARENT_SCOPE)
  else()
    set(TTB_MODDEF_${id}_SKIP_K8S FALSE PARENT_SCOPE)
  endif()

  if (A_IMAGES)
    set(TTB_MODDEF_${id}_IMAGES ${A_IMAGES} PARENT_SCOPE)
  elseif(NOT A_SKIP_OCI)
    set(TTB_MODDEF_${id}_IMAGES ${A_REPO} PARENT_SCOPE)
  endif()

  if (A_TEST_IMAGE)
    set(TTB_MODDEF_${id}_TEST_IMAGE ${A_TEST_IMAGE} PARENT_SCOPE)
  elseif(NOT A_SKIP_K8S)
    set(TTB_MODDEF_${id}_TEST_IMAGE ${A_REPO} PARENT_SCOPE)
  endif()

  if (A_CHARTS)
    set(TTB_MODDEF_${id}_CHARTS ${A_CHARTS} PARENT_SCOPE)
  elseif(NOT A_SKIP_K8S)
    set(TTB_MODDEF_${id}_CHARTS ${TTB_MODDEF_${id}_REPO} PARENT_SCOPE)
  endif()

  if (A_TEST_CHART)
    set(TTB_MODDEF_${id}_TEST_CHART ${A_TEST_CHART} PARENT_SCOPE)
  elseif(NOT A_SKIP_HELM)
    set(TTB_MODDEF_${id}_TEST_CHART ${A_REPO} PARENT_SCOPE)
  endif()

  if (A_HELMFILE_ENV)
    set(TTB_MODDEF_${id}_HELMFILE_ENV ${A_HELMFILE_ENV} PARENT_SCOPE)
  endif()

  set(TTB_MODDEF_${id}_DEPS ${A_DEPS} PARENT_SCOPE)

  foreach(chart ${A_CHARTS})
    name_to_id(${chart} chart_id)
    list(APPEND TTB_CHRTDEF_AVAILABLE_CHARTS ${chart})
    set(TTB_CHRTDEF_AVAILABLE_CHARTS "${TTB_CHRTDEF_AVAILABLE_CHARTS}" PARENT_SCOPE)
    set(TTB_CHRTDEF_${chart_id}_VERSION ${TTB_MODDEF_${id}_VERSION} PARENT_SCOPE)
  endforeach()

  list(APPEND TTB_MODDEF_AVAILABLE_MODULES ${name})
  set(TTB_MODDEF_AVAILABLE_MODULES "${TTB_MODDEF_AVAILABLE_MODULES}" PARENT_SCOPE)
endfunction()

#[=======================================================================[.rst:
.. command:: define_image

  .. signature:: define_image(<image>
                              [IMAGE_ARGS <image_arg>...]
                              [ADDITIONAL_ARGS <arg>...])

  :param <image>: Name of the image
  :param <image_args>: Image build arg to pass to docker,
    converted fully qualified images based on whether the image is defined.
    If the image is not defined, it is pulled from artefacts.skao.int, if
    it is defined, we use the version we build.
  :param <arg>: Additional build args to pass to docker
#]=======================================================================]
function(define_image name)
  name_to_id(${name} id)

  cmake_parse_arguments(A "" "" "IMAGE_ARGS;ADDITIONAL_ARGS" ${ARGN})

  set(TTB_IMGDEF_${id}_IMAGE_ARGS ${A_IMAGE_ARGS} PARENT_SCOPE)
  set(TTB_IMGDEF_${id}_ADDITIONAL_ARGS ${A_ADDITIONAL_ARGS} PARENT_SCOPE)

  list(APPEND TTB_IMGDEF_AVAILABLE_IMAGES ${name})
  set(TTB_IMGDEF_AVAILABLE_IMAGES "${TTB_IMGDEF_AVAILABLE_IMAGES}" PARENT_SCOPE)
endfunction()

function(print_module_def name)
  name_to_id(${name} id)
  message(STATUS "Module ${name}:\n\
    ID=${id}\n\
    TAG=${TTB_MODDEF_${id}_TAG}\n\
    VERSION=${TTB_MODDEF_${id}_VERSION}\n\
    REPO=${TTB_MODDEF_${id}_REPO}\n\
    DEPS=${TTB_MODDEF_${id}_DEPS}\n\
    IMAGES=${TTB_MODDEF_${id}_IMAGES}\n\
    CHARTS=${TTB_MODDEF_${id}_CHARTS}\n\
    SKIP_PYTHON=${TTB_MODDEF_${id}_SKIP_PYTHON}\n\
    SKIP_OCI=${TTB_MODDEF_${id}_SKIP_OCI}\n
    SKIP_HELM=${TTB_MODDEF_${id}_SKIP_HELM}\n
    SKIP_K8S=${TTB_MODDEF_${id}_SKIP_K8S}\n"
    )
endfunction()

define_module(ska-tango-testing TAG 0.6.1 SKIP_HELM SKIP_OCI SKIP_K8S)
define_module(ska-tango-base TAG 0.20.1 DEPS ska-tango-testing SKIP_HELM SKIP_OCI SKIP_K8S)
if ($ENV{CI_JOB_ID})
  set(default_helmfile_env stfc-ci)
else()
  set(default_helmfile_env minikube)
endif()
define_module(ska-low-mccs-common TAG 0.10.2
  DEPS
    ska-tango-testing
    ska-tango-base
    ska-tango-images
  SUBPROJECT
    mccs
  IMAGES
    ska-low-mccs-common
    ska-low-mccs-k8s-test-runner
  CHARTS
    ska-low-mccs-k8s-test-runner
  SKIP_K8S)
define_module(ska-low-mccs-pasd TAG 0.9.0
  DEPS
    ska-tango-base
    ska-low-mccs-common
    ska-tango-images
  SUBPROJECT
    mccs
  TEST_IMAGE ska-low-mccs-k8s-test-runner
  CHARTS
    ska-low-mccs-pasd
  HELMFILE_ENV
    ${default_helmfile_env})
define_module(ska-low-mccs-spshw TAG 0.15.0
  DEPS
    ska-tango-base
    ska-low-mccs-common
    ska-tango-images
  SUBPROJECT
    mccs
  TEST_IMAGE ska-low-mccs-k8s-test-runner
  CHARTS
    ska-low-mccs-spshw
  HELMFILE_ENV
    ${default_helmfile_env})

define_module(ska-tango-images TAG 0.14.15 VERSION 0.4.15
  IMAGES
    ska-tango-images-tango-dependencies
    ska-tango-images-tango-cpp
    ska-tango-images-tango-libtango
    ska-tango-images-tango-admin
    ska-tango-images-tango-databaseds
    ska-tango-images-pytango-builder
    ska-tango-images-pytango-runtime
    ska-tango-images-tango-dsconfig
    ska-tango-images-tango-itango
    ska-tango-images-tango-pytango
    ska-tango-images-tango-java
  CHARTS
    ska-tango-base
    ska-tango-util
  TEST_CHART ska-tango-umbrella
  TEST_IMAGE ska-tango-images-tango-itango
  SKIP_PYTHON)

define_image(ska-tango-images-tango-dependencies)

define_image(ska-tango-images-tango-cpp
  IMAGE_ARGS
    "BUILD_IMAGE=ska-tango-images-tango-dependencies"
  ADDITIONAL_ARGS
  "TANGOIDL_VERSION=${TTB_TANGOIDL_VERSION}"
  "CPPTANGO_VERSION=${TTB_CPPTANGO_VERSION}"
  "TANGO_ADMIN_VERSION=${TTB_TANGO_ADMIN_VERSION}"
  "DATABASEDS_VERSION=${TTB_DATABASEDS_VERSION}")
define_image(ska-tango-images-tango-libtango
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-tango-cpp")
define_image(ska-tango-images-tango-admin
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-tango-libtango")
define_image(ska-tango-images-tango-databaseds
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-tango-libtango")
define_image(ska-tango-images-pytango-builder
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-tango-cpp"
  ADDITIONAL_ARGS
    "PYTANGO_VERSION=${TTB_PYTANGO_VERSION}")
define_image(ska-tango-images-pytango-runtime
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-tango-cpp"
    "BUILD_IMAGE=ska-tango-images-pytango-builder")
define_image(ska-tango-images-tango-dsconfig
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-pytango-runtime"
    "BUILD_IMAGE=ska-tango-images-pytango-builder")
define_image(ska-tango-images-tango-itango
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-pytango-runtime"
    "BUILD_IMAGE=ska-tango-images-pytango-builder")
define_image(ska-tango-images-tango-pytango
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-pytango-runtime"
    "BUILD_IMAGE=ska-tango-images-pytango-builder")
define_image(ska-tango-images-tango-java
  IMAGE_ARGS
    "BUILD_IMAGE=ska-tango-images-tango-dependencies"
  ADDITIONAL_ARGS
    "TANGO_DOWNLOAD_URL=https://gitlab.com/api/v4/projects/24125890/packages/generic/TangoSourceDistribution/${TTB_TSD_VERSION}/tango-${TTB_TSD_VERSION}.tar.gz"
    "TANGO_ADMIN_VERSION=${TTB_TANGO_ADMIN_VERSION}"
  )

define_image(ska-low-mccs-common
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-pytango-runtime"
    "BUILD_IMAGE=ska-tango-images-pytango-builder")

define_image(ska-low-mccs-k8s-test-runner
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-pytango-runtime"
    "BUILD_IMAGE=ska-tango-images-pytango-builder")

define_image(ska-low-mccs-pasd
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-pytango-runtime"
    "BUILD_IMAGE=ska-tango-images-pytango-builder")

define_image(ska-low-mccs-spshw
  IMAGE_ARGS
    "BASE_IMAGE=ska-tango-images-pytango-runtime"
    "BUILD_IMAGE=ska-tango-images-pytango-builder")
