# Build variables for use in bash scripts

include(${TTB_HOOKS_DIR}/default.cmake)
if (EXISTS ${TTB_HOOKS_DIR}/${TTB_MODULE}.cmake)
  include(${TTB_HOOKS_DIR}/${TTB_MODULE}.cmake)
endif()

function(ttb_load_fragment file outvar)
  configure_file(${file}.in ${CMAKE_CURRENT_BINARY_DIR}/${file} @ONLY)
  file(READ ${CMAKE_CURRENT_BINARY_DIR}/${file} out)
  string(REGEX REPLACE "\n$" "" out "${out}")
  set(${outvar} "${out}" PARENT_SCOPE)
endfunction()

if(TTB_MODDEF_${TTB_MODULE_ID}_SUBPROJECT)
  set(TTB_URL "https://gitlab.com/ska-telescope/${TTB_MODDEF_${TTB_MODULE_ID}_SUBPROJECT}/${TTB_REPO}.git")
else()
  set(TTB_URL "https://gitlab.com/ska-telescope/${TTB_REPO}.git")
endif()

# A bash array definition with all the commands
# e.g. something like "(\"help\" \"fetch\" \"build\")"
set(TTB_CMD_ARRAY "")
foreach(script ${TTB_CMD_SCRIPTS})
  get_filename_component(name "${script}" NAME)
  list(APPEND TTB_CMD_ARRAY "\"${name}\"")
endforeach()
string(REPLACE ";" " " TTB_CMD_ARRAY "${TTB_CMD_ARRAY}")
set(TTB_CMD_ARRAY "(${TTB_CMD_ARRAY})")

set(TTB_ROOT ${TTB_DIR}/${TTB_MODULE})
set(TTB_PATCHES ${TTB_PATCHES_DIR}/${TTB_MODULE})

# All dependencies, recursively
set(TTB_ALL_DEPS "")
function(add_to_all_deps mod)
  name_to_id(${mod} id)
  list(APPEND TTB_ALL_DEPS ${mod})
  foreach(dep ${TTB_MODDEF_${id}_DEPS})
    add_to_all_deps(${dep})
  endforeach()
  set(TTB_ALL_DEPS ${TTB_ALL_DEPS} PARENT_SCOPE)
endfunction()
foreach(dep ${TTB_DEPS})
  add_to_all_deps(${dep})
endforeach()
list(REMOVE_DUPLICATES TTB_ALL_DEPS)

# Regular expression matching any dependency or pytango)
set(TTB_DEPS_REGEX "")
# List dependencies to install from the gitlab pypi registry, to be added to
# the end of "poetry add"
set(TTB_INSTALL_DEPS "")
foreach(dep ${TTB_ALL_DEPS})
  name_to_id(${dep} id)
  set(repo ${TTB_MODDEF_${id}_REPO})
  set(skip_python ${TTB_MODDEF_${id}_SKIP_PYTHON})
  if (NOT skip_python)
    if (TTB_INSTALL_DEPS)
      string(APPEND TTB_INSTALL_DEPS " ")
    endif()
    if (TTB_DEPS_REGEX)
      string(APPEND TTB_DEPS_REGEX "|")
    endif()
    string(APPEND TTB_INSTALL_DEPS "\\\n")
    string(APPEND TTB_INSTALL_DEPS
      "  --source ttb ${repo}==0.0.0+dev.c${TTB_COMMIT}")
    string(APPEND TTB_DEPS_REGEX "${dep}")
  endif()
endforeach()

if (TTB_DEPS_REGEX)
  set(TTB_DEPS_AND_PYTANGO_REGEX "pytango|${TTB_DEPS_REGEX}")
else()
  # This regex matches nothing
  set(TTB_DEPS_REGEX "a^")
  set(TTB_DEPS_AND_PYTANGO_REGEX "pytango")
endif()

# List of make commands to build all the images
set(TTB_BUILD_IMAGES_VARS "")
set(images_with_vars "")
function(add_image_var image)
  name_to_id(${image} id)

  list(FIND images_with_vars "${image}" index)
  if (NOT index EQUAL -1)
    return()
  endif()

  list(APPEND images_with_vars ${image})
  set(images_with_vars "${images_with_vars}" PARENT_SCOPE)

  if(TTB_BUILD_IMAGES_VARS)
    string(APPEND TTB_BUILD_IMAGES_VARS "\n")
  endif()

  list(FIND TTB_IMGDEF_AVAILABLE_IMAGES "${image}" index)
  if (index EQUAL -1)
    string(APPEND TTB_BUILD_IMAGES_VARS "${id}=artefact.skao.int/${image}:$(awk -F= '/tag/ {print $2}' images/${image}/.release)")
  else()
    string(APPEND TTB_BUILD_IMAGES_VARS "${id}=${TTB_OCI_REGISTRY}/${image_dep}:${TTB_COMMIT}")
  endif()

  set(TTB_BUILD_IMAGES_VARS "${TTB_BUILD_IMAGES_VARS}" PARENT_SCOPE)
endfunction()

set(TTB_BUILD_IMAGES "")
foreach(image ${TTB_IMAGES})
  name_to_id(${image} id)

  if (TTB_BUILD_IMAGES)
    string(APPEND TTB_BUILD_IMAGES "\n")
  endif()

  list(FIND TTB_IMGDEF_AVAILABLE_IMAGES "${image}" index)
  if (index EQUAL -1)
    message(FATAL_ERROR "Image \"${image}\" not defined")
  endif()

  set(additional_args "")
  foreach(arg ${TTB_IMGDEF_${id}_ADDITIONAL_ARGS})
    if (additional_args)
      string(APPEND additional_args " ")
    endif()
    string(APPEND additional_args "--build-arg ${arg}")
  endforeach()

  foreach(image_arg ${TTB_IMGDEF_${id}_IMAGE_ARGS})
    string(REPLACE "=" ";" image_arg_list ${image_arg})
    list(LENGTH image_arg_list len)

    if (NOT ${len} EQUAL 2)
      message(FATAL_ERROR "Unknown IMAGE_ARG \"${image_arg}\". Expecting NAME=image.")
    endif()

    if (additional_args)
      string(APPEND additional_args " ")
    endif()

    list(GET image_arg_list 0 arg_name)
    list(GET image_arg_list 1 image_dep)
    name_to_id(${image_dep} image_dep_id)

    add_image_var(${image_dep})
    string(APPEND additional_args "--build-arg ${arg_name}=$${image_dep_id}")
  endforeach()

  string(APPEND TTB_BUILD_IMAGES
"make oci-build \\\n\
  CAR_OCI_REGISTRY_HOST=${TTB_OCI_REGISTRY} \\\n\
  CI_COMMIT_SHORT_SHA=${TTB_COMMIT} \\\n\
  OCI_BUILD_ADDITIONAL_ARGS=\"${additional_args}\" \\\n\
  OCI_IMAGE=${image} \\\n\
  OCI_BUILD_ADDITIONAL_TAGS=${TTB_COMMIT}\n")
endforeach()

# Array of images for use in yq
# e.g. ["ska-tango-images-tango-cpp", "ska-tango-images-tango-pytango"]
set(TTB_IMAGE_YQ_ARRAY "")

# Bash array of images
# e.g. (ska-tango-images-tango-cpp ska-tango-images-tango-pytango)
set(TTB_IMAGE_ARRAY "")

foreach(image ${TTB_IMGDEF_AVAILABLE_IMAGES})
  list(APPEND TTB_IMAGE_YQ_ARRAY "\"${image}\"")
  list(APPEND TTB_IMAGE_ARRAY "${image}")
endforeach()
string(REPLACE ";" " "  TTB_IMAGE_ARRAY "${TTB_IMAGE_ARRAY}")
set(TTB_IMAGE_ARRAY "(${TTB_IMAGE_ARRAY})")
string(REPLACE ";" ", "  TTB_IMAGE_YQ_ARRAY "${TTB_IMAGE_YQ_ARRAY}")
set(TTB_IMAGE_YQ_ARRAY "[${TTB_IMAGE_YQ_ARRAY}]")

if ($ENV{TTB_ENABLE_CODER})
  set(TTB_CREATE_CREDENTIALS "make k8s-namespace-credentials")
endif()

# space separated list of charts to build
# e.g. "ska-tango-base ska-tango-util"
set(TTB_CHARTS_TO_BUILD "")
foreach(chart ${TTB_CHARTS})
  list(APPEND TTB_CHARTS_TO_BUILD "${chart}")
endforeach()
string(REPLACE ";" " "  TTB_CHARTS_TO_BUILD "${TTB_CHARTS_TO_BUILD}")

# Array of all charts for use in yq
# e.g. ["ska-tango-base", "ska-tango-util"]
set(TTB_CHARTS_YQ_ARRAY "")
foreach(chart ${TTB_CHRTDEF_AVAILABLE_CHARTS})
  list(APPEND TTB_CHARTS_YQ_ARRAY "\"${chart}\"")
endforeach()
string(REPLACE ";" ", "  TTB_CHARTS_YQ_ARRAY "${TTB_CHARTS_YQ_ARRAY}")
set(TTB_CHARTS_YQ_ARRAY "[${TTB_CHARTS_YQ_ARRAY}]")

# TTB_SETUP_VENV_OR_FETCH will check for venv if the module doesn't SKIP_PYTHON,
# otherwise it will just fetch the module.
if(TTB_MODDEF_${TTB_MODULE_ID}_SKIP_PYTHON)
  set(TTB_SETUP_VENV_OR_FETCH "if ! [ -e \"\${TTB_ROOT}\" ]; then\n  \"${TTB_CMD_SCRIPTS_DIR}/fetch\"\nfi")
else()
  set(TTB_SETUP_VENV_OR_FETCH "if ! [ -e \"\${TTB_ROOT}/.venv\" ]; then\n  \"${TTB_CMD_SCRIPTS_DIR}/setup-venv\"\nfi")
endif()

# TTB_FETCH_PATCH_FRAGMENT will update the pyropject.toml if the module doesn't SKIP_PYTHON
# TTB_FETCH_DESC contains help text describing what fetch does.
set(TTB_FETCH_DESC "Fetch ${TTB_MODULE}.")
set(TTB_FETCH_PATCH_FRAGMENT "")
if(NOT TTB_MODDEF_${TTB_MODULE_ID}_SKIP_PYTHON)
  string(APPEND TTB_FETCH_PATCH_FRAGMENT "\n")
  ttb_load_fragment(fetch-python-end.frag.sh frag)
  string(APPEND TTB_FETCH_PATCH_FRAGMENT "${frag}")
  string(APPEND TTB_FETCH_DESC " Update the pyproject.toml to relax version constraints on\n dependent modules.")
endif()

if (NOT TTB_MODDEF_${TTB_MODULE_ID}_SKIP_HELM)
  if (TTB_FETCH_PATCH_FRAGMENT)
    string(APPEND TTB_FETCH_PATCH_FRAGMENT "\n")
  endif()
  string(APPEND TTB_FETCH_PATCH_FRAGMENT "\n")
  ttb_load_fragment(fetch-helm-end.frag.sh frag)
  string(APPEND TTB_FETCH_PATCH_FRAGMENT "${frag}")
  string(APPEND TTB_FETCH_DESC " Update all Chart.yaml with versions we build for the test bench.")

  if (TTB_HELMFILE_ENV)
    string(APPEND TTB_FETCH_PATCH_FRAGMENT "\nhelmfile -e ${TTB_HELMFILE_ENV} deps")
  endif()
endif()

if (TTB_HELMFILE_ENV)
  ttb_load_fragment(make-myvalues-helmfile-overrides.frag.sh TTB_MYVALUES_OVERRIDES_FRAGMENT)
  string(APPEND TTB_INSTALL_CHART "make k8s-install-chart")
else()
  ttb_load_fragment(make-myvalues-chart-overrides.frag.sh TTB_MYVALUES_OVERRIDES_FRAGMENT)
  set(TTB_INSTALL_CHART "make k8s-install-chart K8S_CHART_EXTRA_PARAMS=\"--values myvalues.yaml\"")
endif()

if (NOT "$ENV{CI_JOB_ID}")
  set(TTB_HELM_BUILD_PUSH_SKIP "HELM_BUILD_PUSH_SKIP=ON")
endif()
